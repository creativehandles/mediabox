<?php

namespace Creativehandles\Mediabox;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\Mediabox\Skeleton\SkeletonClass
 */
class MediaboxFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'mediabox';
    }
}
